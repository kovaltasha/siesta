       module m_efieldp

       use precision
       use fdf
#ifdef MPI
       use mpi
#endif

       implicit none
C       double precision timestep, restarttime
C       integer itimestep, finalstep, initstep
       
       CONTAINS


        subroutine efieldp( cell, ntm, M1, M2, M3, nsm, V)
C ********************************************************************
C Finds the electric potential induced by a gaussian charge density.
C Written by J.M.Pruneda.  2004.
C Modified for distributed V matrix using a 2D grid of processors.
C Routine now is based on intrinsic structure of grid distribution
C in order to calculate position of grid points in space for local
C matrix. 
C *********** INPUT **************************************************
C real*8  cell(3,3)     : Unit cell vectors
C integer ntm(3)        : Global number of divisions of each lattice vector
C integer M1,M2,M3      : Local number of divisions of each lattice vector
C integer nsm           : Number of sub-points for each mesh point
C real*8 param(6)       : Parameters that define the 
C                         charge density: X0(3),q0,alpha
C real*8  X0(3)         : Origin of the point charge
C *********** OUTPUT *************************************************
C real    V(*)          : electric potential at mesh points
C                         Notice single precision in this version
C *********** UNITS **************************************************
C cell  in atomic units (Bohr)
C X0    in atomic units (Bohr)
C V     in atomic units (Ry)
C ********************************************************************
C
C  Modules
C
      use precision
      use parallel
!NK      use elec_dyn_options
      use fdf
#ifdef MPI
      use mpi
#endif
     
      use m_minvec, only: minvec
      
      implicit none

      integer           M1, M2, M3, ntm(3), nsm
      real*8            V(*)
      real*8            cell(3,3), volcel
      external          reclat, volcel

C Internal variables and arrays
      logical           frstme!,found
      integer           I, I1, I2, I3, IX, !NK Node, Nodes,
     .                  ProcessorZ, Py, Pz, I20, I30,
     .                  MG1, MG2, MG3, Yoffset, Zoffset,
     .                  BlockSizeY, BlockSizeZ, NRemY, NRemZ,
     .                  MeshNsm(3), imesh, iu, j1,j2,j3, nn
      real*8            D(3), dvol, DX(3), DX2, Rcell(3,3), X0L(3),PI,
     .                  param(6), Vgrid,
     .                  A, B, C1, C2, C3, C4, yukawa, 
     .                  alpha, q0,derfc,X0(3), cellM(3,3),
     .                   aux(3),Vext, Vp
      real(dp)           B0(3,3)
      save X0,alpha,q0
      data frstme /.true./
#ifdef MPI
      integer           MPIerror
#endif

         call timer('efieldp',1)
C Get the Node number
#ifdef MPI
      call MPI_Comm_Rank(MPI_Comm_World,Node,MPIerror)
      call MPI_Comm_Size(MPI_Comm_World,Nodes,MPIerror)
#else
      Node = 0
      Nodes = 1
#endif


c Find the electric field 
      if (Node.eq.0) then
        call getparam(param)!NK ,0)
        write(6,'(/,a,3f12.6)')
     .    'point charge at ', param(1),param(2),param(3)
      endif

#ifdef MPI
#ifdef NODAT
      call MPI_Bcast(param,5,MPI_double_precision,0,MPI_Comm_World,
     .               MPIerror)
#else
      call MPI_Bcast(param,5,DAT_double,0,MPI_Comm_World,MPIerror)
#endif
#endif
c------------------------------------------------------------

      
C Assign local variables
      PI = 4.D0 * ATAN(1.D0)
      X0(1) = param(1)
      X0(2) = param(2)
      X0(3) = param(3)
      q0 = param(4)
      alpha = param(5)
      yukawa = param(6)
! Yukawa screening parameter
c      C1 = q0*4.d0*pi
      C1 = q0
      C2 = (yukawa*yukawa)*0.25d0/(alpha+1.d-15)
      C3 = sqrt(alpha)
      C4 = yukawa*0.5d0/(C3+1.d-15)

      MG1 = ntm(1)
      MG2 = ntm(2)
      MG3 = ntm(3)


C Find minimum cell
      call minvec(B0,cell,cellM)           !NK added B0

C Find reciprocal cell vectors (without the factor 2*pi)
      call reclat( cellM, Rcell, 0 )


C Find origin in lattice coordinates
      do I = 1,3
        X0L(I)= X0(1)*Rcell(1,I) + X0(2)*Rcell(2,I) + X0(3)*Rcell(3,I)
      enddo

C Check that ProcessorY is a factor of the number of processors
      if (mod(Nodes,ProcessorY).gt.0) then
        write(6,'(''ERROR: ProcessorY must be a factor of the'',
     .    '' number of processors!'')')
        stop
      endif
      ProcessorZ = Nodes/ProcessorY

C Calculate coarse mesh sizes
      MeshNsm(1) = MG1/nsm
      MeshNsm(2) = MG2/nsm
      MeshNsm(3) = MG3/nsm

C Calculate blocking sizes
      BlockSizeY = (MeshNsm(2)/ProcessorY)*nsm
      NRemY = (MG2 - ProcessorY*BlockSizeY)/nsm
      BlockSizeZ = (MeshNsm(3)/ProcessorZ)*nsm
      NRemZ = (MG3 - ProcessorZ*BlockSizeZ)/nsm

C Calculate coordinates of current node in processor grid
      Py = (Node/ProcessorZ)+1
      Pz = Node - (Py - 1)*ProcessorZ + 1

C Calculate starting point for grid
      Yoffset = (Py-1)*BlockSizeY + nsm*min(Py-1,NRemY)
      Zoffset = (Pz-1)*BlockSizeZ + nsm*min(Pz-1,NRemZ)

      imesh = 0
      I30 = Zoffset 
      do I3 = 1,M3
        I30 = I30 + 1

        I20 = Yoffset
        do I2 = 1,M2
          I20 = I20 + 1
 
          do I1 = 1,M1
            do i=1,3
              aux(I)=dble(I1-1)*cell(i,1)/ dble(MG1)+
     .         dble(I20-1)*cell(i,2)/ dble(MG2)+
     .         dble(I30-1)*cell(i,3)/ dble(MG3)
            enddo 
            do i=1,3
              D(I)=aux(1)*Rcell(1,i)+aux(2)*Rcell(2,i)+
     .        aux(3)*Rcell(3,i)
              D(I)=D(I)-X0L(I)
              D(I)=D(I)-nint(D(I))
            enddo 
           
            imesh=imesh+1
C DSP sum up to the inmediate neighbors to make the potential
C periodic assuming that it is short ranged
            Vgrid=0.0d0
C Number of shells of neighbors
            nn=2
            do j1=-nn,nn
            do j2=-nn,nn
            do j3=-nn,nn
            DX(1) = cellM(1,1)*D(1) + cellM(1,2)*D(2) + 
     .        cellM(1,3)*D(3)+
     .        j1*cellM(1,1)+j2*cellM(1,2)+j3*cellM(1,3)
            DX(2) = cellM(2,1)*D(1) + cellM(2,2)*D(2) + 
     .        cellM(2,3)*D(3)+
     .        j1*cellM(2,1)+j2*cellM(2,2)+j3*cellM(2,3)
            DX(3) = cellM(3,1)*D(1) + cellM(3,2)*D(2) + 
     .        cellM(3,3)*D(3)+
     .        j1*cellM(3,1)+j2*cellM(3,2)+j3*cellM(3,3)
            DX2 = DX(1)**2 + DX(2)**2 + DX(3)**2
            A = sqrt(DX2)
            B = A*C3
C..... Potential of screened (Yukawa) gaussian charge distribution.........
            Vp=  C1 * ( exp(C2+A*yukawa)*derfc(C4+B) -
     .           exp(C2-A*yukawa)*derfc(C4-B) ) / (2.d0*A+1.d-15)
C ...... correcting asymptotic behaviour
            Vp=2.0d0*Vp
 
C..... Potential of an hydrogenoic density + gaussian nuclei ..............
!           Vp=
!     .          C1 * ( ( derfc(B) - derfc(-B) ) / (2.d0*A+1.d-15)
!     .                 - exp(-2.d0*A)*(2.d0*A*A+2.d0*A+1.d0)/(A+1.d-15)
!     .                 + 1.d0/(A+1.d-15) )
C.... Using the function extpot
c          Vp=Vext(A)

           Vgrid=Vgrid+Vp
          enddo 
          enddo 
          enddo 
          V(imesh)=V(imesh)+Vgrid

          enddo
        enddo
      enddo
      frstme =.false.
      call timer('efieldp',2) 

      end



          double precision function Vext(R)

          use interpolation, only: spline
          use interpolation, only: splint
          use interpolation, only: polint

          use precision
          use fdf
          implicit none
          double precision  R, dVdr 
          double precision, dimension(:), allocatable
     .      :: Vread, Rread
          double precision, dimension(:), allocatable, 
     .      save :: Vint,V2 
          double precision x,dx,y,dy, yp1,ypn, Rmax
          integer unit1, itb, ntbmax, nr, ir, 
     .      npoint, nmin, nmax, nn, nread
          logical frstme, exist1
          save ntbmax, frstme, Rmax
          data frstme /.true./


C         Define the distance between points 
C         in the radial grid, and npoint for interpolation
          dx=0.01d0
          npoint=4

          if(frstme) then 
             inquire(file='extpot',exist=exist1)
             if(exist1) then 
             call io_assign(unit1)
             open(unit=unit1,file='extpot', form='formatted') 
                read(unit1,*) nread
                allocate(Vread(nread))
                allocate(Rread(nread))
                do ir=1,nread
                    read(unit1,*) Rread(ir),Vread(ir)
                enddo 
             close(unit1)
             else
                stop 'Efieldp: file extpot not found'
             endif
C interpolate in an homogeneous radial grid
             Rmax=Rread(nread)
             ntbmax=nint(Rread(nread)/dx)+1
             allocate(Vint(ntbmax))
             allocate(V2(ntbmax))
             nr=1
             do itb=1,ntbmax-1
                x=dx*(itb-1)
                do ir=nr,nread
                   if(Rread(ir).lt.x) then
                          nr=ir
                   else
                      goto 20
                   endif
                enddo 
  20            continue
                nmin=max(1,nr-npoint)
                nmax=min(nread,nr+npoint)
                nn=nmax-nmin+1
                call polint(Rread(nmin),Vread(nmin),nn,x,y,dy)
                Vint(itb)=y
            enddo 
            deallocate(Rread)
            deallocate(Vread)
            yp1=huge(1.d0)
            ypn=huge(1.d0)
            call spline(dx,Vint,ntbmax,yp1,ypn,V2)
            frstme=.false.
           endif
           if(r.gt.Rmax) then 
                Vext=0.0d0
           else
            call splint(dx,Vint,V2,ntbmax,r,y,dVdr)
            Vext= y
           endif  
          end

CCC******************************************************************          
CC Soubroute setting parameters for the point charge.
CC Originally from version 1.2.20 (2003), module "elec_dyn_options".
CC Added and modified by Natalia Koval, 2020
         
      subroutine getparam(inputs)!,im)
      use m_initwf, only: initwf
      use fdf
      double precision inputs(6),V(3),param(6),origin(3)
      integer i   !iu,im
      real(dp) totime
      integer istpp

      type(block_fdf) :: bfdf
      type(parsed_line), pointer :: pline
      
      logical frstme,frstmv,found
      save frstme,frstmv,V,param,origin
      data frstme /.true./
      data frstmv /.true./
      call initwf( istpp,totime)
      write(*,*) 'print time in m_efieldp, totime=', totime
c Find and store the electric field only the first time
      if (frstme) then
        frstme = .false.
      endif  
!        found = fdf_block('PointCharge', iu)
      if (fdf_block('PointCharge', bfdf) ) then
        if (.not. fdf_bline(bfdf, pline))
     .    call die('redcel: ERROR in PointCharge block')
        do i=1,3
          origin(i)=fdf_bvalues(pline,1)
          V(i)=fdf_bvalues(pline,2)
        enddo
        do i=4,6
          param(i)=fdf_bvalues(pline,3)
        enddo
!        if(found) then
!         read(iu,*) origin(1),origin(2),origin(3)
!         read(iu,*) V(1),V(2),V(3)
!         read(iu,*) param(4),param(5),param(6)         
      endif
      call fdf_bclose(bfdf)
      
      param(1) = origin(1) + V(1)*totime                ! tottime()
      param(2) = origin(2) + V(2)*totime
      param(3) = origin(3) + V(3)*totime
      write(6,*) 'getparam: Time, Xion: '
      write(6,*) totime,param(1),param(2),param(3)
      inputs = param
      return
      end subroutine getparam

C      double precision function tottime()
CC Returns total simulations time
C         tottime = (itimestep-initstep)*timestep +
C     .         restarttime
C         return
C      end function tottime

       end module m_efieldp
